const http = require('http');
const PORT = 4422;

function server(req, res) {
    const { url } = req;

    res.end(
	url === '/' ? 'Main' :
	url === '/about' ? 'About' :	
	'404'
    )
}

http.createServer(server).listen(PORT);
