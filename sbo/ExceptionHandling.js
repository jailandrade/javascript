function getPerson(id) {
	if (id < 0 ) {
		throw new Error('ID must not be negative: ' + id);
	}
	return { id: id }	
}

function getPersons(ids) {
	var result = [];
	ids.forEach(function(id) {
		try {
			var person = getPerson(id);
			result.push(person);
		} catch (exception) {
			console.error(exception);
		}
	});
	return result;
}

console.info(getPersons([1, -1, 2]));

