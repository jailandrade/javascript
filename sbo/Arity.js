function pair(x, y){
	if (arguments.length !== 2) {
		throw new Error('Need exactly 2 arguments');
	}

	x = x || 0;
	y = y || 0;

	return [x, y];
}

//console.info(pair());
//console.info(pair(3));
console.info(pair(3,2));
