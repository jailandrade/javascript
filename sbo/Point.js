function Point(x,y) {
	this.x = x;
	this.y = y;
}

Point.prototype.dist = function() {
	return Math.sqrt(this.x*this.x + this.y*this.y);
}

var p = new Point(3, 5);
console.info(p.x);
console.info(p.dist());
console.info(p instanceof Point);
