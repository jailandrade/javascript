var arr = ['a', 'b', 'c'];

console.info(arr.slice(1,2));
console.info(arr.slice(1));

arr.push('x');
console.info(arr);

arr.pop();
console.info(arr);

arr.shift();
console.info(arr);

arr.unshift();
console.info(arr);

console.info(arr.indexOf('b'));
console.info(arr.indexOf('y'));

console.info(arr.join('-')); // all elements in a single string
console.info(arr.join('')); 
console.info(arr.join());

['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'].forEach(
	function(elem, index) {
		console.log(index + ' . ' + elem);
	});

console.info([1, 2, 3, 4, 5].map(function(x) {
	return x * x;
}));

console.info(/^a+b+$/.test('aaab'));
console.info(/^a+b+$/.test('aaaa'));

console.info(/a(b+)a/.exec('_abbba_aba_'));
